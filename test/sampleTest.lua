local somethingWeAreTesting
local thisThrowsError

function beforeEach.prepare()
    somethingWeAreTesting = function(a, b)
        if a < 0 or b < 0 then return end

        return a + b
    end

    thisThrowsError = function()
        error("Sample error message")
    end
end

function test.addition()
    local a = 1
    local b = 2

    local output = somethingWeAreTesting(a, b)

    assertEquals(3, output)
end

function test.negativeValuesReturnNil()
    local a = 1
    local b = -1

    local output = somethingWeAreTesting(a, b)

    assertNil(output)
end

function test.thisTestShouldFail()
    assertTrue(false)
end

function test.verifyThatSomethingThrowsAnError()
    local expectedErrorMessage = "Sample error message"

    assertThrows(expectedErrorMessage, function() thisThrowsError() end)
end