--[[
    key = test class name
    value = path to test class
]]
return {
    sampleTest = "test/sampleTest",
    otherTest = "test/otherTest"
}