# Unit test framework for Lua

## How to run
Install a rock with `luarocks make`

Run it with: `lua -e "require('pilcrow.unito')('test/definitions')"`

Or for a specific class: `lua -e "require('pilcrow.unito')('test/definitions', 'sampleTest')"`

Or specific test: `lua -e "require('pilcrow.unito')('test/definitions', 'sampleTest', 'addition')"`